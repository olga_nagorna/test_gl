//variables
var menu = document.getElementById('menu');
var window_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var window_height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
var menu_width = 150, menu_height = 168; //menu_height = 96; - with scroll
var scroll_position = 0, min_value = -48, max_value = -24;

// event handlers
document.addEventListener('click', onContextMenu, false);
document.addEventListener('mouseenter', openNextLevel, true);
document.addEventListener('mouseleave', closeNextLevel, true);
document.addEventListener('click', handleItem, false);
document.addEventListener('keydown', checkKey, false);

function getSubItems(subitems){
    if (subitems == null) {
        return '';
    }
    var submenu = [];
    for(var i = 0; i < subitems.length; i++) {
        var item = subitems[i].item.name + '<ul>' + getSubItems(subitems[i].item.submenu) + '</ul>';
        var classname = ((subitems[i].item.disabled == 'true') ? 'disabled': '');
        //add class to decorate with arrows
        //TODO: adding class for next levels
        if(subitems[i].item.submenu !==null){
            classname += 'has_submenu';
        }
        submenu += '<li class=' + classname + '>' + item + '</li>';
    }
    return submenu
}

function getAndParseJson() {
    var xhr = new XMLHttpRequest();
    var parsedItems;
    xhr.open('GET', 'menu.json', true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if(xhr.status == 200) {
                parsedItems = JSON.parse(xhr.responseText);
                formMenu(parsedItems);
            }
        }
    };
    xhr.send();
}

function formMenu(menuItems) {
    var menu = getSubItems(menuItems);
    renderItems(menu);
}

function renderItems(menu) {
    document.getElementById('menu').innerHTML = menu;
}

getAndParseJson();

function openContextMenu(x, y){
    //TODO: calculate depth of submenus and their width and height
    if(x > window_width - menu_width){
        x = window_width - menu_width - 9;
        menu.classList.add('left_side_submenu');
    }
    if(y > window_height - menu_height){
        y = window_height - menu_height;
        menu.classList.add('up_side_submenu');
    }
    menu.style.left = x - 1 + 'px';
    menu.style.top = y - 18 + 'px';
    menu.style.display = 'block';
    menu.classList.add('opened_context_menu');
}

function closeContextMenu(){
    menu.classList.remove('opened_context_menu');
    menu.classList.remove('left_side_submenu');
    menu.classList.remove('up_side_submenu');
    menu.style.display = 'none';
}

function onContextMenu(e){
    openContextMenu(e.pageX, e.pageY);
    document.addEventListener('click', onClick, false);
}

function onClick(e){
    closeContextMenu();
    document.removeEventListener('click', onClick);
}

menu.addEventListener('click',function(e){
    e.stopPropagation();
},false);


function openNextLevel(e){
    if (e.target.nodeName == 'LI') {
        e.preventDefault();
        var child = e.target.children[0];
        if(child.childNodes.length > 0){
            child.style.display="block";
            child.style.border="1px solid rgba(51, 51, 51, .35)";
        }
    }
}

function closeNextLevel(e){
    if (e.target.nodeName == 'LI') {
        var child = e.target.children[0];
        child.style.display="none";
    }
}

function handleItem(e){
    if (e.target.nodeName == 'LI') {
        var child = e.target.children[0];
        if(child.childNodes.length > 0){
            child.style.display="block";
            child.style.border="1px solid rgba(51, 51, 51, .35)";
        }
    }
}

function checkKey(e) {
    e = e || window.event;
    if (e.keyCode == '38') {
        // up arrow
        scrollUp(e);
    }
    else if (e.keyCode == '40') {
        // down arrow
        scrollDown(e);
    }
}
//keys work, but I disabled overflow hidden and height (96px - height of 4 items) for menu,
// because overflow hidden does not match for multiple level submenu - horizontal scroll appears
//TODO: refactor menu rendering
function scrollUp(e){
    if(scroll_position > max_value) return false
    else{
        scroll_position += 24;
        for (var i = 0; i < document.getElementById('menu').childNodes.length; i++){
            document.getElementById('menu').childNodes[i].style.top=scroll_position+'px';
        }
    }
}

function scrollDown(e){
    if(scroll_position < min_value){
        return false;
    }
    else {
        scroll_position -= 24;
        for (var i = 0; i < document.getElementById('menu').childNodes.length; i++) {
            document.getElementById('menu').childNodes[i].style.top = scroll_position + 'px';
        }
    }
}